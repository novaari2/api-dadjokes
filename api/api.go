package api

import (
	"dadjokeapi/entity"
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"

	"github.com/gofiber/fiber/v2"
)

var URL = "https://icanhazdadjoke.com/"

func GetRandomJokes(c *fiber.Ctx) error {
	req, err := http.NewRequest("GET", URL, nil)

	if err != nil {
		log.Fatal(err)
	}

	req.Header.Set("Accept", "application/json")

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		log.Fatal(err)
	}

	defer resp.Body.Close()

	jsonBytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatal(err)
	}

	var jokereceh entity.Jokes

	er := json.Unmarshal(jsonBytes, &jokereceh)

	if er != nil {
		log.Fatal(err)
	}

	return c.Status(fiber.StatusOK).JSON(fiber.Map{
		"Id":     jokereceh.Id,
		"Joke":   jokereceh.Joke,
		"status": jokereceh.Status,
	})
}

func GetJoksById(c *fiber.Ctx) error {
	id := c.Params("*")

	req, err := http.NewRequest("GET", URL+"j/"+id, nil)

	if err != nil {
		log.Fatal(err)
	}

	req.Header.Set("Accept", "application/json")

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		log.Fatal(err)
	}

	defer resp.Body.Close()

	jsonBytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatal(err)
	}

	var jokereceh entity.Jokes

	er := json.Unmarshal(jsonBytes, &jokereceh)

	if er != nil {
		log.Fatal(err)
	}

	return c.Status(fiber.StatusOK).JSON(fiber.Map{
		"Id":     jokereceh.Id,
		"Joke":   jokereceh.Joke,
		"status": jokereceh.Status,
	})
}
