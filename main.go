package main

import (
	"dadjokeapi/api"
	"log"

	"github.com/gofiber/fiber/v2"
)

func setupRoutes(app *fiber.App) {
	app.Get("api/joke", api.GetRandomJokes)
	app.Get("api/joke/*", api.GetJoksById)
}

func main() {
	app := fiber.New()

	setupRoutes(app)

	log.Fatal(app.Listen(":3000"))
}
